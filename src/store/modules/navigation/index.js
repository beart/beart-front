import Cookies from 'js-cookie'

// better call mutations only from actions

const navigation = {
  namespaced: true,
  state: () => ({
      navOpened: Cookies.get('navOpened') ? Cookies.get('navOpened') != "false" : false
  }),
  mutations: {
    // sync state change. Without call to api. 
    SET_NAV_OPENED(state, setVal) {
      Cookies.set('navOpened', setVal, {
        expires: 365,
        path: '/'
      })
      state.navOpened = setVal
    }
  },
  actions: {
    // async state change. chaging state by call mutations
    setNav(context, payload) {
      context.commit('SET_NAV_OPENED', payload);
    }
  },
  getters: {
    // complex getters. for example get list of user by specific filter
    // 
    // simple example
    // getTodoById: (state) => (id) => {
    //   return state.todos.find(todo => todo.id === id)
    // }
  }
}

export default navigation;