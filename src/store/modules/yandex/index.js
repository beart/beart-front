import api from 'utils/api';

const navigation = {
  namespaced: true,
  state: () => ({
    data: {}
  }),
  mutations: {
    // sync state change. Without call to api. 
    SET_COUNTER(state, payload) {
      state.data[payload.id] = payload.data;
    }
  },
  actions: {
    // async state change. chaging state by call mutations
    async getData(context, payload) {
      console.log(payload);

      var res = await api.get(`yandex/counter/${payload.id}`);
      
      context.commit('SET_COUNTER', {
        id: payload.id,
        data: res
      });
    }
  },
  getters: {
    // complex getters. for example get list of user by specific filter
    getMF: (state) => (id) => {
      const counter = state.data[id] || null;

      var filtered = {};

      if(counter) {
        filtered = counter.data[Object.keys(counter.data)[0]].data.reduce((prev, curr)=>{
          if (prev[curr.dimensions[0].name] == undefined) {
            prev[curr.dimensions[0].name] = 0
          } else {
            prev[curr.dimensions[0].name] += curr.totals[0]
          }

          return prev;
        }, {});
      }

      var data = {}
      data.labels = Object.keys(filtered).filter(key => Number(filtered[key]));
      data.series = data.labels.map(key=>filtered[key])

      return {
        ...data
      }
    }
  }
}

export default navigation;